<!DOCTYPE html>
<html>
<head>
    <title>SignUp Page</title>
    <link href="css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
    <div id="header">
        <h3>Sign Up</h3>
    </div>
    <div id="wrap">
    	<?php
    	require 'PHPMailer\PHPMailer\src\PHPMailer.php';
        require 'PHPMailer\PHPMailer\src\SMTP.php';
        require 'PHPMailer\PHPMailer\src\Exception.php';
//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
        use PHPMailer\PHPMailer\PHPMailer;
        use PHPMailer\PHPMailer\SMTP;
        use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
        require 'vendor/autoload.php';

//Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);
    	error_reporting(-1);
        ini_set('display_errors', 'On');
        set_error_handler("var_dump");
        ini_set("mail.log", "/tmp/mail.log");
        ini_set("mail.add_x_header", TRUE);
        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "Myseproject";
        $conn = new mysqli($servername, $username, $password, $dbname);
        if(isset($_POST['name'])&&!empty($_POST['name']) AND isset($_POST['email'])&&!empty($_POST['email'])){
            $name=mysqli_real_escape_string($conn,$_POST['name']);
            $email=mysqli_real_escape_string($conn,$_POST['email']);
            if(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i", $email)){
                $msg = 'The email you have entered is invalid, please try again.';
            }
            else{
                $msg = 'Your account has been made, <br /> please verify it by clicking the activation link that has been send to your email.';
                $hash=md5(rand(0,1000));
                $password=rand(1000,5000);
                $sql="INSERT INTO Users (username , password, email, hash) VALUES ('".mysqli_real_escape_string($conn,$name)."','".mysqli_real_escape_string($conn,password_hash($password, PASSWORD_DEFAULT))."','".mysqli_real_escape_string($conn,$email)."','".mysqli_real_escape_string($conn,$hash)."')";
                $query=mysqli_query($conn,$sql);
                try {
                //Server settings
                $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output
                $mail->isSMTP();                                            //Send using SMTP
                $mail->Host       = 'smtp.gmail.com';                     //Set the SMTP server to send through
                $mail->SMTPAuth   = true;                                   //Enable SMTP authentication
                $mail->Username   = 'sethuvamsikrishna@gmail.com';                     //SMTP username
                $mail->Password   = 'vamsirockz';                               //SMTP password
                $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
                $mail->Port       = 587;                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

                //Recipients
                $mail->setFrom('sethuvamsikrishna@gmail.com', 'Mailer');
                //$mail->addAddress(strval($email), 'sethuvamsikrishna');     //Add a recipient
                $mail->AddAddress($_POST['email']);               //Name is optional
                //$mail->addReplyTo('.$email.', 'Information');

                //Attachments
                //$mail->addAttachment('1.jpg');         //Add attachments  //Optional name

                //Content
                $mail->isHTML(true);                                  //Set email format to HTML
                $mail->Subject = 'Signup|Verification';
                $mail->Body    = '
                Thanks for Signing up!<br>
                Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.<br>
                ------------------------<br>
                Username:'.$name.'<br>
                Password:'.$password.'<br>
                ------------------------<br>

                Please click this link to activate your account:<br>
                http://localhost\php programs\verify.php?email='.$email.'&hash='.$hash.'<br>
                http://www.yourwebsite.com/verifyphp?email='.$email.'&hash='.$hash.'<br>
                ';
                $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

                $mail->send();
                //$to=$email;
                //$subject='Signup|Verification';
                //$message='
                //Thanks for Signing up!
                //Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below.
                //------------------------
                //Username:'.$name.'
                //Password:'.$password.'
                //------------------------

                //Please click this link to activate your account:
                //http://www.yourwebsite.com/verifyphp?email='.$email.'&hash='.$hash.'
                //';          
                //$headers='From: sethuvamsikrishna@gmail.com'."\r\n";
                //mail($to,$subject,$message);
                echo 'Message has been sent';
                } catch (Exception $e) {
                echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
                }
            }
        }
        ?>
        <h3>Signup Form</h3>
        <p>Please enter your name and email address to create your account</p>
        <?php 
            if(isset($msg)){ 
                echo '<div class="statusmsg">'.$msg.'</div>';
            } 
        ?>
        <form action="" method="post">
            <label for="name">Name:</label>
            <input type="text" name="name" value=""/>
            <label for="email">Email:</label>
            <input type="text" name="email" value=""/>

            <input type="submit" class="submit_button" value="Sign up"/>
        </form>
    </div>
</body>
</html>
